-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2020-09-29 16:50:51
-- 服务器版本： 5.6.48-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xyq`
--

-- --------------------------------------------------------

--
-- 表的结构 `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL,
  `title` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `content` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `link` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `myorder` int(11) DEFAULT '0',
  `addip` char(20) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `addtime` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1267 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `notes`
--

INSERT INTO `notes` (`id`, `title`, `content`, `link`, `myorder`, `addip`, `addtime`) VALUES
(1256, 'helloworld', '你好，世界。', 'http://www.icixi.com', 0, '0', '2016-12-23 09:59:34'),
(1257, '做一个善良的人', '做一个善良的人', '#', 0, '0', '2016-12-23 10:00:07'),
(1263, 'ming', '现阶段我们对新申请邮箱的用户，都会在WEB界面 -发现设置中-勾选“保留WEB发信”“保留客户端发信”，也请各位同事在配置OUTLOOK 或FOXMAIL的时候 勾选“在服务器上保留邮件副本', '#', 1, '0', '2016-12-23 10:03:04'),
(1264, 'helloworlda', '如果不能授人以鱼，你是否愿意授人以渔;', '#', 0, '0', '2016-12-23 10:03:32'),
(1259, 'helloworlds', '如果有一天，你成为技术的大牛，你会不会想起，那个曾经指点过你的朋友;', '#', 0, '0', '2016-12-23 10:01:25'),
(1260, '如果有一', '如果有一天，当技术要金钱交换，你会不会怀念，那时我们纯真到无所不谈;', '#', 0, '0', '2016-12-23 10:01:48'),
(1262, 'wang', '如果有一天，有人问了你曾经请教过别人的问题，你会不会愿意，倾囊相授;', '#', 0, '0', '2016-12-23 10:02:38'),
(1265, 'liming', '自反而缩，虽千万人吾往矣。', '#', 0, '0', '2016-12-23 10:04:05'),
(1266, '本站域名出售', 'icixi.com 域名出售，有意可联系QQ:10260087\r\n\r\nicixi.com Domain name for sale, interested can contact QQ:10260087', '#', 0, '0', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1267;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
